(function($) {

    Vue.component('cryptoid-shortcodes', {
        template: '#cryptoid-shortcodes-template',
        props: ['options'],
        data: function() {
            var data = {
                coin: 'BTCIDR',
                exchange: 'Indodax'
            };
            return data;
        },
        mounted: function() {

            var self = this;

            $('.form-select').selectize({
                persist: false,
                create: false,
                maxItems: 1,
                dropdownParent: 'body',
                onChange: function(value) {
                    var model = $(this.$input).attr('name');
                    self[model] = value;
                }
            });

            var cp = new ClipboardJS('.copy-btn');

        }
    });

    Vue.component('cryptoid-widgets', {
        template: '#cryptoid-widgets-template',
        props: ['options'],
        data: function() {
            var data = {
                type: 'card',
                design: 'price-slider',
                coin: 'BTCIDR',
                exchange: 'Indodax',
                theme: 'light',
                transparent: false,
                width: 380,
                height: 560,
                responsive: false,
                token: cryptoid.apitoken,
                sizes: {
                    card: {
                        'price-slider': { w: 380, h: 560 },
                        'simple-price': { w: 480, h: 150 },
                        'price-chart': { w: 480, h: 135 },
                        'price-icon': { w: 470, h: 135 },
                        'exchange-price': { w: 480, h: 145 },
                        'exchange-buy-sell': { w: 480, h: 226 },
                        'price-table': { w: 470, h: 406 },
                        'price-buy-sell-table': { w: 470, h: 375 },
                    },
                    ticker: {
                        default: { w: 800, h: 38 }
                    },
                    table: {
                        default: { w: 800, h: 520 }
                    },
                    chart: {
                        default: { w: 470, h: 580 }
                    },
                    button: {
                        default: { w: 360, h: 46 }
                    }
                }
            };
            return data;
        },
        watch: {
            type: function() {
                this.design = (this.type == 'card') ? 'price-slider' : 'default';
                this.width = this.sizes[this.type][this.design]['w'];
                this.height = this.sizes[this.type][this.design]['h'];
                this.apply();
            },
            design: function() {
                this.width = this.sizes[this.type][this.design]['w'];
                this.height = this.sizes[this.type][this.design]['h'];
            }
        },
        computed: {
            embedCode: function() {
                var code = '[cryptoid type="' + this.type + '" design="' + this.design + '" theme="' + this.theme + '" coin="' + this.coin + '" exchange="' + this.exchange + '"  width="' + this.width + '" height="' + this.height + '" responsive="' + this.responsive + '" transparent="' + this.transparent + '"]';
                return code;
            },
            showCryptocurrency: function() {
				var show = false;
				if (this.type == 'card' && ['simple-price', 'price-chart', 'price-icon', 'exchange-price', 'exchange-buy-sell', 'price-table', 'price-buy-sell-table'].includes(this.design)) {
					show = true;
				}
				return show;
			},
			showExchange: function() {
				var show = false;
				if (this.type == 'card' && ['exchange-price'].includes(this.design)) {
					show = true;
				}
				return show;
			}
        },
        mounted() {

            var self = this;

            $('.form-select').selectize({
                persist: false,
                create: false,
                maxItems: 1,
                dropdownParent: 'body',
                onChange: function(value) {
                    var model = $(this.$input).attr('name');
                    self[model] = value;
                }
            });

            var cp = new ClipboardJS('#embedcodebtn');

            this.apply();

        },
        methods: {
            apply: function() {

                $('.cryptocurrency-id-widget').empty();
				$('.cryptocurrency-id-widget').css('margin', '0 auto').css('width', this.width + 'px');
				$('.cryptocurrency-id-widget').html(this.getIframe());

            },
            reset: function() {

            },
            getIframe: function() {
				var url = cryptoid.endpoint + 'embed/' + this.type + '/' + this.design + '?token=' + this.token + '&coin=' + this.coin + '&exchange=' + this.exchange + '&theme=' + this.theme + '&transparent=' + this.transparent;
				return '<iframe scrolling="no" allowtransparency="true" frameborder="0" src="' + url + '" style="box-sizing: border-box; height: calc(' + this.height + 'px); width: ' + this.width + 'px;"></iframe>';
			},
            copyEmbedCode: function() {

            }
        }
    });

    var app = new Vue({
        el: '.vue-component',
        props: {
            options: Object
        }
    });

})( jQuery );